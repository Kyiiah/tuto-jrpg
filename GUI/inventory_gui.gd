extends Control

signal opened
signal closed

var isOpen: bool = false

@onready var inventory: Inventory = preload("res://inventory/playerInventory.tres")
@onready var ItemStackGuiClass = preload("res://GUI/itemsStackGui.tscn")
@onready var slots: Array = $NinePatchRect/GridContainer.get_children()

var itemInHand : ItemStackGui

func _ready():
	connectSlots() #pour dire manitenant les slots ont cette écoute
	inventory.updated.connect(update)
	update()
	
func connectSlots():
	for i in range(slots.size()):
		var slot = slots[i]
		slot.index = i
		
		var callable = Callable(onSlotClicked) #pour savoir quel slot est appelé, on crée une var callable
		callable = callable.bind(slot) #on met .bind (parametre)
		slot.pressed.connect(callable) #permet de passer le parametre à la fonction appelé ici on SlotClicked
		#calable appelle la fonction avec le parametre (sinon on peut pas le faire avec le signal directement)

func update():
	for i in range(min(inventory.slots.size(), slots.size())):
		var inventorySlot: InventorySlot = inventory.slots[i]
	
		if !inventorySlot.item: continue
	
		var itemStackGui: ItemStackGui = slots[i].itemStackGui
		if !itemStackGui:
			itemStackGui = ItemStackGuiClass.instantiate()
			slots[i].insert(itemStackGui)
		
		itemStackGui.inventorySlot = inventorySlot
		itemStackGui.update()
		
		

func open():
	visible = true
	isOpen = true
	opened.emit()
	
	
func close():
	visible = false
	isOpen = false
	closed.emit()

func onSlotClicked(slot):
	if slot.isEmpty() && itemInHand: #si itemInHand est null alors cest false
		insertItemInSlot(slot)
		return
	if !itemInHand:
		takeItemFromSlot(slot)

func takeItemFromSlot(slot):
	itemInHand = slot.takeItem()
	add_child(itemInHand)
	updateItemInHand()

func insertItemInSlot(slot):
	var item = itemInHand
	remove_child(itemInHand)
	itemInHand = null
	
	slot.insert(item)
	
func updateItemInHand():
	if !itemInHand: return
	itemInHand.global_position = get_global_mouse_position() - itemInHand.size/2

func _input(event):
	updateItemInHand()
	
