extends HBoxContainer

@onready var HeartGuiClass = preload("res://GUI/heartGUI.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func setMaxHearts(max: int):
	for i in range(max) :
		var heart = HeartGuiClass.instantiate()
		add_child(heart)

func updateHearts(currentHealth: int):
	var hearts = get_children()
	
	for i in range(currentHealth):
		hearts[i].update(true)
	
	for i in range(currentHealth, hearts.size()):  
		#ex : si 2 vies, une eteinte, de currentHealth 2 à heart.size() 3, ca fait i prend valeur 2 et cest tout, donc un coeur éteint
		#si 1 vie restant, deux éteintes. de 1 à 3, i prend valeur 1 puis 2 et donc éteint 2 coeurs. (range (inclusif, exclusif)
		#les enfants commencent à 0
		hearts[i].update(false)
