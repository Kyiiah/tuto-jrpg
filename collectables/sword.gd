extends "res://collectables/Collectable.gd"

@onready var animations = $AnimationPlayer

func collect(inventory: Inventory):
	animations.play("spin")
	await animations.animation_finished
	super.collect(inventory) #appelle le script du script supérieur (hérité) on joue dabord le collec() d'ici, puis le collect sup du script collectables
	
