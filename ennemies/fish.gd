extends CharacterBody2D

@export var speed = 20
@export var limit = 0.5
@export var endPoint: Marker2D

@onready var animations = $AnimatedSprite2D

var startPosition
var endPosition


func _ready():
	startPosition = position
	endPosition = endPoint.global_position

func updateVelocity():
	var moveDirection = endPosition - position  
	#position est un vecteur qui change à haque instant (appelé dans physic process), et qui diminue en y car on va vers le haut (negatif sur godot)
	#il se rapproche peu à peu des coordonnées endPosition, donc le resultat est plus en plus proche d'un vecteur nul
	#jusqua etre nul quand la flame est à endPosition, endPosition - endPosition = 0. 
	#la velocité étant basé sur ce resultat, elle devient peu à peu nulle.
	if moveDirection.length() < limit :
		changeDirection()
	velocity = moveDirection.normalized() * speed

func updateAnimation():
	if velocity.length() == 0:
		if animations.is_playing():
			animations.stop()
	else:
		var direction = "Down"
		if velocity.x < 0: direction = "Left"
		elif velocity.x > 0: direction = "Right"
		elif velocity.y < 0: direction = "Up"
		
		animations.play("walk"+direction)

func changeDirection():
	var tempEnd = endPosition
	endPosition = startPosition
	startPosition = tempEnd

func _physics_process(delta):
	updateVelocity()
	move_and_slide()
	updateAnimation()
